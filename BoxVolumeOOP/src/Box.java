
public class Box {

    double height;
    double width;
    double depth;
    Box(double x,double y, double z){
        height=x;
        width=y;
        depth=z;
    }
    void displayVol(){
        double volume;
        volume=height*width*depth;
        System.out.println("Volume of the box: "+volume);
    }

}
